package com.company.graph2;

import java.util.ArrayList;
import java.util.Comparator;


public class Vertex {
    int weight;
    int firstV;
    int secondV;

    public Vertex() {
    }

    public Vertex(int weight, int firstV, int secondV) {
        this.weight = weight;
        this.firstV = firstV;
        this.secondV = secondV;
    }

    public int getWeight() {
        return weight;
    }

    public void sorting(ArrayList<Vertex> arr) {
        arr.sort(new Comparator<Vertex>() {
            @Override
            public int compare(Vertex v1, Vertex v2) {
                return v1.weight - v2.getWeight();

            }
        });
    }

}
