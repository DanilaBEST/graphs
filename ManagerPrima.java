package com.company.graph2;

import java.util.Scanner;

public class ManagerPrima {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        GraphTasks2Solution graphTasks2Solution = new GraphTasks2Solution();
        int n = scanner.nextByte();
        scanner.nextLine();
        int[][] adjacencyMatrix = new int[n][n];
        entrance(scanner, n, adjacencyMatrix);
        System.out.println(graphTasks2Solution.primaAlgorithm(adjacencyMatrix));
    }

    static void entrance(Scanner scanner, int n, int[][] adjacencyMatrix) {
        for (int i = 0; i < n; i++) {
            String s = scanner.nextLine();
            int column = 0;
            int discharge = 0;
            boolean flag = false;
            for (int j = 0; j < s.length(); j++) {
                if (s.charAt(j) >= '0') {
                    if (discharge == 0) {
                        discharge += s.charAt(j) - 48;
                    } else {
                        discharge *= 10;
                        discharge += s.charAt(j) - 48;
                    }
                    flag = true;
                } else if (flag) {
                    flag = false;
                    adjacencyMatrix[i][column] = discharge;
                    column++;
                    discharge = 0;
                }
            }
            if (flag) {
                adjacencyMatrix[i][column] = discharge;
            }
        }
    }

}
