package com.company.graph2;

import java.util.Scanner;

public class ManagerDijkstra {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[][] g = new int[n][n];
        scanner.nextLine();
        ManagerPrima.entrance(scanner, n, g);
        int startV = scanner.nextInt();
        GraphTasks2Solution graphTasks2Solution = new GraphTasks2Solution();
        System.out.println(graphTasks2Solution.dijkstraSearch(g, startV));
    }

}
