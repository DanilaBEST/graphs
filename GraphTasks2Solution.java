package com.company.graph2;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static java.lang.Math.min;
import static java.util.Arrays.fill;

    public class GraphTasks2Solution implements GraphTasks2 {
        @Override
        public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
            HashMap<Integer, Integer> answer = new HashMap<>();
            ArrayDeque<Integer> arrayDeque = new ArrayDeque<>();
            boolean[] usedV = new boolean[adjacencyMatrix[0].length];
            int[] way = new int[adjacencyMatrix[0].length];
            arrayDeque.offer(startIndex);
            usedV[startIndex] = true;
            while (!arrayDeque.isEmpty()) {
                int v = arrayDeque.pollFirst();
                for (int i = 0; i < adjacencyMatrix[0].length; i++) {
                    if (adjacencyMatrix[v][i] > 0 && !usedV[i]) {
                        arrayDeque.offer(i);
                        usedV[i] = true;
                        way[i] = way[v] + adjacencyMatrix[v][i];
                    }
                    if (adjacencyMatrix[v][i] > 0 && usedV[i] && way[v] + adjacencyMatrix[v][i] < way[i]) {
                        arrayDeque.offer(i);
                        way[i] = way[v] + adjacencyMatrix[v][i];
                    }
                }

            }
            for (int i = 1; i < adjacencyMatrix[0].length; i++) {
                if (i != startIndex) {
                    answer.put(i, way[i]);
                }
            }
            return answer;    }

        @Override
        public Integer primaAlgorithm(int[][] adjacencyMatrix) {
            int INF = Integer.MAX_VALUE / 2;
            int[] dist = new int[adjacencyMatrix[0].length];
            boolean[] used = new boolean[adjacencyMatrix[0].length];
            fill(dist, INF);
            dist[0] = 0;
            for (int i = 0; i < adjacencyMatrix[0].length; ++i) {
                int v = -1;
                for (int j = 0; j < adjacencyMatrix[0].length; ++j) {
                    if (!used[j] && dist[j] < INF && (v == -1 || dist[v] > dist[j])) {
                        v = j;
                    }
                }
                if (v == -1) {
                    System.out.println("no way");
                    System.exit(0);
                }
                used[v] = true;
                for (int to = 0; to < adjacencyMatrix[0].length; to++) {
                    if (!used[to] && adjacencyMatrix[v][to] < INF && adjacencyMatrix[v][to] != 0) {
                        dist[to] = min(dist[to], adjacencyMatrix[v][to]);
                    }
                }
            }
            return toGetSum(dist);
        }

        @Override
        public Integer krascalAlgo(ArrayList<Vertex> g, int[] iVert) {
            int answer = 0;
            for (Vertex vertex : g) {
                if (iVert[vertex.firstV] != iVert[vertex.secondV]) {
                    int old_id = iVert[vertex.secondV];
                    int new_id = iVert[vertex.firstV];
                    answer += vertex.weight;
                    for (int j = 0; j < iVert.length; ++j) {
                        if (iVert[j] == old_id) {
                            iVert[j] = new_id;
                        }
                    }
                }
            }
            return answer;
        }

        public int toGetSum(int... distance) {
            return Arrays.stream(distance).sum();
        }
    }
