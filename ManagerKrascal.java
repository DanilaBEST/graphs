package com.company.graph2;

import java.util.ArrayList;
import java.util.Scanner;

public class ManagerKrascal {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Vertex vertex = new Vertex();
        int n = scanner.nextInt();
        int[] iVert = new int[n];
        ArrayList<Vertex> g = new ArrayList<>();
        scanner.nextLine();
        for (int i = 0; i < n; i++) {
            String s = scanner.nextLine();
            iVert[i] = i;
            int column = 0;
            int discharge = 0;
            boolean flag = false;
            for (int j = 0; j < s.length(); j++) {
                if (s.charAt(j) >= '0') {
                    if (discharge == 0) {
                        discharge += s.charAt(j) - 48;
                    } else {
                        discharge *= 10;
                        discharge += s.charAt(j) - 48;
                    }
                    flag = true;
                } else if (flag) {
                    flag = false;
                    if (discharge != 0) {
                        Vertex storage = new Vertex(discharge, i, column);
                        g.add(storage);
                    }
                    column++;
                    discharge = 0;
                }
            }
            if (flag) {
                if (discharge != 0) {
                    Vertex storage = new Vertex(discharge, i, column);
                    g.add(storage);
                }
            }
        }
        GraphTasks2Solution graphTasks2Solution = new GraphTasks2Solution();
        vertex.sorting(g);
        System.out.println(graphTasks2Solution.krascalAlgo(g, iVert));
    }

}
